# Kamera

Kamera is a simple and customizable camera module to include in your android application.



![camera preview in tv screen](bigben_tvscreen.png)



Features:

- Captures pictures and videos


- Automatically handle Lollipop+ permissions


- Allows still pictures compression


- Media preview capability with accept and cancel options


- Allows icons visibility change by code


- Allows icons change by code


- Can be included in your app as a module


- Allows mirroring mode in Y axis (To keep selfie orientation, for example)


- Allows setting media destination path


- Allows you to modify picture as you want before requesting accept/cancel action and saving


- Add image overlay to camera preview (So if you want to add a frame, for example, the user can see the final result)


- Open source

And many more!



![image preview edited by code](overlayed_media_preview.png)





This project is a simple example about how to use Kamera module. Many customizations can be made from initial activity just by changing switch states.

### How to make it work?

- Add following permissions to your manifest:
  `<uses-permission android:name="android.permission.CAMERA" />`

  `<uses-permission android:name="android.permission.RECORD_AUDIO"/>`

  `<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />`

- Add following activity to your manifest:
  `<activity android:name="com.kodevincere.kamera.ui.KameraActivity"/>`

### How to configure the camera?

There is a singleton class called `KameraConfig`, by getting its instance, you will be able to configure the camera.

### Available configuration:



`setInitialCamera(int initialCamera); `

Description: Allows you to define the initial camera in the preview.
Allowed Values: `INITIAL_CAMERA_BACK`,` INITIAL_CAMERA_FRONT`
Default Value: `INITIAL_CAMERA_BACK`



`setResourceBack(int resourceBack);`

Description: Allows you to define the back button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceCapture(int mesourceCapture);`

Description: Allows you to define the capture button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceRecord(int resourceRecord);`

Description: Allows you to define the record button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceSwitch(int resourceSwitch);`

Description: Allows you to define the switch camera button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceAccept(int resourceAccept);`

Description: Allows you to define the accept button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceCancel(int resourceCancel);`

Description: Allows you to define the cancel button resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setResourceOverlay(int resourceOverlay);`

Description: Allows you to define the overlay resource resource by code.
Allowed Values: Any drawable or mipmap from your app.
Default Value: `DEFAULT_RESOURCE`



`setAspectRatio(double mAspectRatio);`

Description: Allows you to define the camera aspect ratio by code.
Allowed Values: `ASPECT_RATIO_WIDESCREEN`, `ASPECT_RATIO_TVSCREEN`
Default Value: `ASPECT_RATIO_WIDESCREEN`



`setBackButtonVisible(boolean backButtonVisible);`

Description: Allows you to define the back button visibility status by code.
Allowed Values: `true`, `false`
Default Value: `false`



`setCameraButtonsBackgroundVisible(boolean cameraButtonsBackgroundVisible);`

Description: Allows you to define the camera buttons background visibility status by code (Adds a shadow to allow buttons visibility if camera preview is similar to buttons color).
Allowed Values: `true`, `false`
Default Value: `false`



`setMediaPreviewButtonsBackgroundVisible(boolean mediaPreviewButtonsBackgroundVisible);`

Description: Allows you to define the media preview buttons background visibility status by code (Adds a shadow to allow buttons visibility if media preview is similar to buttons color).
Allowed Values: `true`, `false`
Default Value: `false`



`setOverlayVisible(boolean mOverlayVisible);`

Description: Allows you to define the overlay picture visibility status by code.
Allowed Values: `true`, `false`
Default Value: `false`



`setCompress(boolean mCompress);`

Description: Allows you to define if still pictures must be compressed before saving or not.
Allowed Values: `true`, `false`
Default Value: `false`



`setAutoAccept(boolean mAutoAccept);`

Description: Allows you to define if captured media must be auto accepted or needs user confirmation.
Allowed Values: `true`, `false`
Default Value: `false`



`setCaptureButtonVisible(boolean mCaptureButtonVisible);`

Description: Allows you to define the capture button visibility status by code.
Allowed Values: `true`, `false`
Default Value: `false`



`setSwitchCameraButtonVisible(boolean mSwitchCameraButtonVisible);`

Description: Allows you to define the switch camera button visibility status by code.
Allowed Values: `true`, `false`
Default Value: `false`



`setMirrorImageInYAxisAlways(boolean mMirrorImageInYAxis);`

Description: Allows you to define if still pictures must be always mirrored in Y axis.
Allowed Values: `true`, `false`
Default Value: `false`



`setMirrorInYAxisOnlyWhenFrontCamera(boolean mMirrorOnlyWhenFrontCamera);`

Description: Allows you to define if still pictures must be mirrored in Y axis only when taken with front camera (Perfect for selfies).
Allowed Values: `true`, `false`
Default Value: `false`



`setMediaType(String mMediaType);`

Description: Allows you to define if camera will take still pictures or will record video.
Allowed Values: `MEDIA_TYPE_PICTURE`, `MEDIA_TYPE_VIDEO`
Default Value: `MEDIA_TYPE_PICTURE`



`setDestinationPath(String mDestinationPath);`

Description: Allows you to define the captured media destination path (Saving location in device).
Allowed Values: Any valid device path.
Default Value: N/A



`setPictureEditorListener(KameraActivity.PictureEditorListener pictureEditorListener)`

Description: Allows you to define a PictureEditorListener for receiving taken still pictures to be modified by you as you want
Allowed Values: Any object implementing `KameraActivity.PictureEditorListener` interface
Default Value: `null`



![Camera preview widescreen with some buttons enabled](bigben_widescreen.png)



## License

```
Copyright 2016 KodeVincere

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```









