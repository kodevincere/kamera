package com.example.me.camera.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.me.camera.R;
import com.kodevincere.kamera.config.KameraConfig;
import com.kodevincere.kamera.fileutils.ImageHelper;
import com.kodevincere.kamera.ui.KameraActivity;
import com.kodevincere.kamera.ui.KameraActivity.PictureEditorListener;
import com.kodevincere.kamera.views.KameraView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements PictureEditorListener, Switch.OnCheckedChangeListener {

    private ImageView ivImage;

    private Switch swFrontCamera, swWidescreen, swVideo, swCompressPicture,
            swOverlayPicture, swAutoacceptMedia, swBackButtonVisible,
            swSwitchCameraVisible, swAlwaysMirror, swFrontMirror;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton picture = (FloatingActionButton) findViewById(R.id.picture);

        findViews();
        setListeners();

        ivImage = (ImageView) findViewById(R.id.iv_image);

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                configureCamera();

                Intent i = new Intent(MainActivity.this, KameraActivity.class);
                startActivityForResult(i, 123);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == 123) {
                Bundle b = data.getExtras();
                String mediaLocation = b.getString(KameraActivity.INTENT_KEY_SAVED_MEDIA, "Error");
                String mediaType = b.getString(KameraActivity.INTENT_KEY_MEDIA_TYPE);

                Toast.makeText(MainActivity.this, "Path: " + mediaLocation, Toast.LENGTH_SHORT).show();

                Bitmap bm = null;
                ImageHelper imageHelper = new ImageHelper();

                if(mediaType.equals(KameraActivity.INTENT_VALUE_MEDIA_TYPE_PICTURE)) {
                    bm = imageHelper.loadReducedBitmap(mediaLocation, ivImage.getWidth(), ivImage.getHeight());
                    bm = imageHelper.loadBitmapWithCorrectOrientation(bm, mediaLocation);
                }else if(mediaType.equals(KameraActivity.INTENT_VALUE_MEDIA_TYPE_VIDEO)) {
                    bm = imageHelper.loadVideoBitmap(mediaLocation);
                }


                ivImage.setImageBitmap(bm);

            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        switch (id){
            case R.id.sw_video_mode:
                boolean enabled = true;
                if(isChecked) {
                    enabled = false;
                    swCompressPicture.setChecked(false);
                    swOverlayPicture.setChecked(false);
                }
                swCompressPicture.setEnabled(enabled);
                swOverlayPicture.setEnabled(enabled);
                break;
            case R.id.sw_mirror_front:
                enabled = true;
                if(isChecked){
                    enabled = false;
                    swAlwaysMirror.setChecked(false);
                }
                swAlwaysMirror.setEnabled(enabled);
                break;
        }
    }

    @Override
    public void onPictureReadyToEdit(String picturePath, KameraConfig.OnPictureModifiedListener pictureModifiedListener) {
        Bitmap b = BitmapFactory.decodeFile(picturePath);

        pictureModifiedListener.onPictureModified(getOverlayedPicture(b));
    }

    private void findViews(){
        swFrontCamera = (Switch)findViewById(R.id.sw_init_front_camera);
        swWidescreen = (Switch)findViewById(R.id.sw_init_widescreen);
        swVideo = (Switch)findViewById(R.id.sw_video_mode);
        swCompressPicture = (Switch)findViewById(R.id.sw_compress_picture);
        swOverlayPicture = (Switch)findViewById(R.id.sw_overlay_picture);
        swAutoacceptMedia = (Switch)findViewById(R.id.sw_auto_accept_media);
        swBackButtonVisible = (Switch)findViewById(R.id.sw_back_button_visible);
        swSwitchCameraVisible = (Switch)findViewById(R.id.sw_switch_camera_visible);
        swAlwaysMirror = (Switch)findViewById(R.id.sw_mirror_always);
        swFrontMirror = (Switch)findViewById(R.id.sw_mirror_front);
    }

    private void setListeners(){
        swVideo.setOnCheckedChangeListener(this);
        swFrontMirror.setOnCheckedChangeListener(this);
    }

    private void configureCamera(){
        KameraConfig kameraConfig = KameraConfig.getInstance();
        kameraConfig.initializeConfig();

        int initialCamera = KameraConfig.INITIAL_CAMERA_BACK;
        double initialAspectRatio = KameraConfig.ASPECT_RATIO_TVSCREEN;
        String mediaType = KameraConfig.MEDIA_TYPE_PICTURE;
        boolean compress = false;
        boolean autoAccept = false;
        boolean switchCameraButtonVisible = false;

        if(swFrontCamera.isChecked())
            initialCamera = KameraConfig.INITIAL_CAMERA_FRONT;
        if(swWidescreen.isChecked())
            initialAspectRatio = KameraConfig.ASPECT_RATIO_WIDESCREEN;
        if(swVideo.isChecked())
            mediaType = KameraConfig.MEDIA_TYPE_VIDEO;
        if(swCompressPicture.isChecked())
            compress = true;
        if(swAutoacceptMedia.isChecked())
            autoAccept = true;
        if(swSwitchCameraVisible.isChecked())
            switchCameraButtonVisible = true;


        kameraConfig.setInitialCamera(initialCamera)
                .setAspectRatio(initialAspectRatio)
                .setMediaType(mediaType)
                .setCompress(compress)
                .setAutoAccept(autoAccept)
                .setSwitchCameraButtonVisible(switchCameraButtonVisible)
                .setDestinationPath(Environment.getExternalStorageDirectory().getAbsoluteFile().toString().concat(File.separator).concat(Long.toString(System.currentTimeMillis())));

        if(swOverlayPicture.isChecked())
            kameraConfig.setOverlayVisible(true)
                    .setResourceOverlay(R.drawable.birthday_frame)
                    .setPictureEditorListener(MainActivity.this);

        if(swBackButtonVisible.isChecked()) {
            kameraConfig.setBackButtonVisible(true)
                    .setResourceBack(R.drawable.ic_arrow_back_white_48dp);
        }

        if(swAlwaysMirror.isChecked())
            kameraConfig.setMirrorImageInYAxisAlways(true);

        if(!swFrontMirror.isChecked())
            kameraConfig.setMirrorInYAxisOnlyWhenFrontCamera(false);
    }

    public Bitmap getOverlayedPicture(Bitmap bm) {
        Bitmap bm1 = BitmapFactory.decodeResource(getResources(), R.drawable.birthday_frame).copy(Bitmap.Config.ARGB_8888, true);

        Bitmap overlayedBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), Bitmap.Config.ARGB_8888);

        bm = Bitmap.createScaledBitmap(bm, overlayedBitmap.getWidth(), (int) (overlayedBitmap.getWidth() * KameraView.PICTURE_RATIO_TVSCREEN), true);

        int marginLeft = 0;
        int marginTop = overlayedBitmap.getHeight()/2 - bm.getHeight()/2;

        Canvas localCanvas = new Canvas(overlayedBitmap);

        localCanvas.drawBitmap(bm, marginLeft, marginTop, null);
        bm.recycle();

        localCanvas.drawBitmap(bm1, 0, 0, null);
        bm1.recycle();

        drawText(localCanvas, "KodeVincere!", 450, 350);

        System.gc();
        return overlayedBitmap;
    }

    public void drawText(Canvas canvas, String text, int height, int width) {
        Paint paint = initPaint();
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int x = ((canvas.getWidth() / 2) - (bounds.width() / 2)) + width;
        canvas.drawText(text, x, height, paint);
    }

    private Paint initPaint(){
//        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/profile_font.ttf");
        Paint p = new Paint();
        p.setColor(Color.DKGRAY);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setTextSize(120);
//        p.setTypeface(tf);
        return p;
    }

}
