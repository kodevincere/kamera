package com.kodevincere.kamera.config;

import android.graphics.Bitmap;
import android.hardware.Camera;

import com.kodevincere.kamera.views.KameraView;
import com.kodevincere.kamera.ui.KameraActivity;

/**
 * Created by alphonselric
 */
public class KameraConfig {

    public static int INITIAL_CAMERA_BACK = Camera.CameraInfo.CAMERA_FACING_BACK;
    public static int INITIAL_CAMERA_FRONT = Camera.CameraInfo.CAMERA_FACING_FRONT;
    public static double ASPECT_RATIO_WIDESCREEN = KameraView.PICTURE_RATIO_WIDESCREEN;
    public static double ASPECT_RATIO_TVSCREEN = KameraView.PICTURE_RATIO_TVSCREEN;
    public static String MEDIA_TYPE_PICTURE = "picture";
    public static String MEDIA_TYPE_VIDEO = "video";


    private static KameraConfig ourInstance = new KameraConfig();
    public static final int DEFAULT_RESOURCE = -1;
    private int mInitialCamera = INITIAL_CAMERA_BACK;
    private int mResourceBack = DEFAULT_RESOURCE;
    private int mResourceCapture = DEFAULT_RESOURCE;
    private int mResourceRecord = DEFAULT_RESOURCE;
    private int mResourceSwitch = DEFAULT_RESOURCE;
    private int mResourceAccept = DEFAULT_RESOURCE;
    private int mResourceCancel = DEFAULT_RESOURCE;
    private int mResourceOverlay = DEFAULT_RESOURCE;
    private double mAspectRatio = ASPECT_RATIO_WIDESCREEN;
    private boolean mBackButtonVisible = false;
    private boolean mCameraButtonsBackgroundVisible = false;
    private boolean mMediaPreviewButtonsBackgroundVisible = false;
    private boolean mOverlayVisible = false;
    private boolean mCompress = false;
    private boolean mAutoAccept = true;
    private boolean mCaptureButtonVisible = true;
    private boolean mSwitchCameraButtonVisible = true;
    private boolean mMirrorImageInYAxisAlways = false;
    private boolean mMirrorImageInYAxisOnlyWhenFrontCamera = true;
    private String mMediaType = MEDIA_TYPE_PICTURE;
    private String mDestinationPath = "";
    private KameraActivity.PictureEditorListener sPictureEditorListener;

    public static KameraConfig getInstance() {
        return ourInstance;
    }

    private KameraConfig() {

    }

    public KameraConfig setInitialCamera(int mInitialCamera) {
        this.mInitialCamera = mInitialCamera;
        return this;
    }

    public KameraConfig setResourceBack(int resourceBack){
        this.mResourceBack = resourceBack;
        return this;
    }

    public KameraConfig setResourceCapture(int mesourceCapture){
        this.mResourceCapture = mesourceCapture;
        return this;
    }

    public KameraConfig setResourceRecord(int resourceRecord){
        mResourceRecord = resourceRecord;
        return this;
    }

    public KameraConfig setResourceSwitch(int resourceSwitch){
        mResourceSwitch = resourceSwitch;
        return this;
    }

    public KameraConfig setResourceAccept(int resourceAccept){
        mResourceAccept = resourceAccept;
        return this;
    }

    public KameraConfig setResourceCancel(int resourceCancel){
        mResourceCancel = resourceCancel;
        return this;
    }

    public KameraConfig setResourceOverlay(int resourceOverlay){
        mResourceOverlay = resourceOverlay;
        return this;
    }

    public KameraConfig setAspectRatio(double mAspectRatio) {
        this.mAspectRatio = mAspectRatio;
        return this;
    }

    public KameraConfig setBackButtonVisible(boolean backButtonVisible) {
        this.mBackButtonVisible = backButtonVisible;
        return this;
    }

    public KameraConfig setCameraButtonsBackgroundVisible(boolean cameraButtonsBackgroundVisible) {
        this.mCameraButtonsBackgroundVisible = cameraButtonsBackgroundVisible;
        return this;
    }

    public KameraConfig setMediaPreviewButtonsBackgroundVisible(boolean mediaPreviewButtonsBackgroundVisible) {
        this.mMediaPreviewButtonsBackgroundVisible = mediaPreviewButtonsBackgroundVisible;
        return this;
    }

    public KameraConfig setOverlayVisible(boolean mOverlayVisible) {
        this.mOverlayVisible = mOverlayVisible;
        return this;
    }

    public KameraConfig setCompress(boolean mCompress) {
        this.mCompress = mCompress;
        return this;
    }

    public KameraConfig setAutoAccept(boolean mAutoAccept) {
        this.mAutoAccept = mAutoAccept;
        return this;
    }

    public KameraConfig setCaptureButtonVisible(boolean mCaptureButtonVisible) {
        this.mCaptureButtonVisible = mCaptureButtonVisible;
        return this;
    }

    public KameraConfig setSwitchCameraButtonVisible(boolean mSwitchCameraButtonVisible) {
        this.mSwitchCameraButtonVisible = mSwitchCameraButtonVisible;
        return this;
    }

    public KameraConfig setMirrorImageInYAxisAlways(boolean mMirrorImageInYAxis) {
        this.mMirrorImageInYAxisAlways = mMirrorImageInYAxis;
        return this;
    }

    public KameraConfig setMirrorInYAxisOnlyWhenFrontCamera(boolean mMirrorOnlyWhenFrontCamera) {
        this.mMirrorImageInYAxisOnlyWhenFrontCamera = mMirrorOnlyWhenFrontCamera;
        return this;
    }

    public KameraConfig setMediaType(String mMediaType) {
        this.mMediaType = mMediaType;
        return this;
    }

    public KameraConfig setDestinationPath(String mDestinationPath) {
        this.mDestinationPath = mDestinationPath;
        return this;
    }

    public KameraConfig setPictureEditorListener(KameraActivity.PictureEditorListener pictureEditorListener){
        sPictureEditorListener = pictureEditorListener;
        return this;
    }


    public int getInitialCamera() {
        return mInitialCamera;
    }

    public int getResourceBack() {
        return mResourceBack;
    }

    public int getResourceCapture() {
        return mResourceCapture;
    }

    public int getResourceRecord() {
        return mResourceRecord;
    }

    public int getResourceSwitch() {
        return mResourceSwitch;
    }

    public int getResourceAccept() {
        return mResourceAccept;
    }

    public int getResourceCancel() {
        return mResourceCancel;
    }

    public int getResourceOverlay() {
        return mResourceOverlay;
    }

    public double getAspectRatio() {
        return mAspectRatio;
    }

    public boolean isBackButtonVisible() {
        return mBackButtonVisible;
    }

    public boolean isCameraButtonsBackgroundVisible() {
        return mCameraButtonsBackgroundVisible;
    }

    public boolean isMediaPreviewButtonsBackgroundVisible() {
        return mMediaPreviewButtonsBackgroundVisible;
    }

    public boolean isOverlayVisible() {
        return mOverlayVisible;
    }

    public boolean isCompress() {
        return mCompress;
    }

    public boolean isAutoAccept() {
        return mAutoAccept;
    }

    public boolean isCaptureButtonVisible() {
        return mCaptureButtonVisible;
    }

    public boolean isSwitchCameraButtonVisible() {
        return mSwitchCameraButtonVisible;
    }

    public boolean isMirrorImageInYAxis() {
        return mMirrorImageInYAxisAlways;
    }

    public boolean isMirrorImageInYAxisOnlyWhenFrontCamera() {
        return mMirrorImageInYAxisOnlyWhenFrontCamera;
    }

    public String getMediaType() {
        return mMediaType;
    }

    public String getDestinationPath() {
        return mDestinationPath;
    }

    public KameraActivity.PictureEditorListener getPictureEditorListener() {
        return sPictureEditorListener;
    }

    public void initializeConfig(){
        mInitialCamera = INITIAL_CAMERA_BACK;
        mResourceBack = DEFAULT_RESOURCE;
        mResourceCapture = DEFAULT_RESOURCE;
        mResourceRecord = DEFAULT_RESOURCE;
        mResourceSwitch = DEFAULT_RESOURCE;
        mResourceAccept = DEFAULT_RESOURCE;
        mResourceCancel = DEFAULT_RESOURCE;
        mResourceOverlay = DEFAULT_RESOURCE;
        mAspectRatio = KameraView.PICTURE_RATIO_WIDESCREEN;
        mBackButtonVisible = false;
        mCameraButtonsBackgroundVisible = false;
        mMediaPreviewButtonsBackgroundVisible = false;
        mOverlayVisible = false;
        mCompress = false;
        mAutoAccept = true;
        mCaptureButtonVisible = true;
        mSwitchCameraButtonVisible = true;
        mMirrorImageInYAxisAlways = false;
        mMirrorImageInYAxisOnlyWhenFrontCamera = true;
        mMediaType = MEDIA_TYPE_PICTURE;
        mDestinationPath = "";
        sPictureEditorListener = null;
    }

    public interface OnPictureModifiedListener{
        void onPictureModified(Bitmap bitmap);
    }
}
