package com.kodevincere.kamera.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.kodevincere.kamera.R;

/**
 * Created by alphonselric
 */

public class DrawingView extends View {
    private boolean haveTouch = false;
    private Rect touchArea;
    private Paint paint;
    private Bitmap focusRect;
    private long lastTouchTime;


    public DrawingView(Context context) {
        super(context);
        initDrawingView(context);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initDrawingView(context);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initDrawingView(Context context){
        paint = new Paint();
        paint.setColor(0xeed7d7d7);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        haveTouch = false;

        focusRect = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_focus_rect);
    }

    public void setHaveTouch(boolean val, Rect rect) {

        if (val || System.currentTimeMillis() - lastTouchTime >= 1500) {
            if (val) {
                lastTouchTime = System.currentTimeMillis();
            }
            haveTouch = val;
            touchArea = rect;
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (haveTouch) {
            canvas.drawBitmap(focusRect,
                    touchArea.left, touchArea.top,
                    paint);
        }
    }
}