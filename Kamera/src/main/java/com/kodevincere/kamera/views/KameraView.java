package com.kodevincere.kamera.views;

/**
 * Created by alphonselric
 */

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kodevincere.kamera.MyOrientationEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class KameraView extends TextureView implements TextureView.SurfaceTextureListener{

    private String LOG_TAG = this.getClass().getCanonicalName();

    public static String[] requiredPermissions = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final double PICTURE_RATIO_WIDESCREEN = 16.0/9;
    public static final double PICTURE_RATIO_TVSCREEN = 4.0/3;
    public static final String FLASH_MODE_ON = Camera.Parameters.FLASH_MODE_ON;
    public static final String FLASH_MODE_OFF = Camera.Parameters.FLASH_MODE_OFF;
    public static final String FLASH_MODE_AUTO = Camera.Parameters.FLASH_MODE_AUTO;
    public static int VIDEO_DURATION = 120;
    public static final int SNAP_DURATION = 20;

    private DrawingView drawingView;

    private Camera mCamera;
    private int mActualCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private int mAvailableCameras;
    private SurfaceTexture mSurfaceTexture;
    private Camera.AutoFocusCallback myAutoFocusCallback;
    private Camera.PictureCallback jpegCallback;
    private boolean isRecording;
    private MediaRecorder mR;
    private CameraPreviewListener cpListener;
    private String initialFlashMode;
    private double desiredAspectRatio = PICTURE_RATIO_WIDESCREEN;
    private int lastPictureRotation = -1;

    private String videoPath, videoName;

    private byte[] pictureArray;

    private List<Camera.Size> mSupportedVideoSizes;

    //////////////////////////////////////////////Constructores, deben existir por el hecho de extender de View//////////////////////////////////////////////

    public KameraView(Context context) {
        super(context);
    }

    public KameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //////////////////////////////////////////////Fin constructores//////////////////////////////////////////////


    //////////////////////////////////////////////Métodos a sobreescribir//////////////////////////////////////////////
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mSurfaceTexture = surface;
        cpListener.onSurfaceTextureAvailable();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        releaseCameraAndPreview();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    //////////////////////////////////////////////Fin métodos a sobreescribir//////////////////////////////////////////////


    //////////////////////////////////////////////Métodos necesarios para iniciar el funcionamiento//////////////////////////////////////////////


    public void setListener(CameraPreviewListener cPListener) {
        this.cpListener = cPListener;
    }

    public void finishConstruction(String initialFlashMode, double desiredAspectRatio, int initialCameraId, boolean addDrawingView){

        if(cpListener == null)
            throw new IllegalStateException("You must set CameraPreviewListener");

        this.initialFlashMode = initialFlashMode;
        this.desiredAspectRatio = desiredAspectRatio;

        if(initialCameraId == Camera.CameraInfo.CAMERA_FACING_BACK || initialCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
            this.mActualCameraId = initialCameraId;

        this.setSurfaceTextureListener(this);

        if(checkPermissions()) {
            if(addDrawingView)
                createDrawingView();

            createCallbacks();
            checkCameraNumber();

            cpListener.updateLayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        }else{
            requestPermissions();
        }
    }

    public void finishConstruction(String initialFlashMode, double desiredAspectRatio) throws IllegalStateException {
        finishConstruction(initialFlashMode, desiredAspectRatio, Camera.CameraInfo.CAMERA_FACING_BACK, true);
    }

    //////////////////////////////////////////////Fin métodos necesarios para iniciar el funcionamiento//////////////////////////////////////////////

    //////////////////////////////////////////////Inicializadores de elementos internos//////////////////////////////////////////////

    private void createDrawingView() {
        drawingView = new DrawingView(getContext());
        try {
            ViewGroup parent = (ViewGroup) getParent();
            if (parent != null)
                parent.addView(drawingView, new FrameLayout.LayoutParams(getWidth(), getHeight()));
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void createCallbacks() {
        jpegCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {

                pictureArray = data;

                camera.stopPreview();

                cpListener.onPictureTaken(pictureArray, mActualCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT);

            }
        };

        myAutoFocusCallback = new Camera.AutoFocusCallback() {

            @Override
            public void onAutoFocus(boolean arg0, Camera arg1) {
                if (arg0) {
                    mCamera.cancelAutoFocus();
                    setFocusMode(mCamera.getParameters(), Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                }
            }
        };
    }

    synchronized public void getCameraInformation() {

        isRecording = false;
        try {
            mCamera = getCameraInstance();

            if (mCamera != null) {
                Camera.Parameters parameters = mCamera.getParameters();

                mSupportedVideoSizes = parameters.getSupportedVideoSizes();

                if(mSupportedVideoSizes == null)
                    mSupportedVideoSizes = parameters.getSupportedPreviewSizes();

            }

        } catch (Exception ex){
            cpListener.onCameraError(ex);
        }
    }

    private void checkCameraNumber() {
        mAvailableCameras = Camera.getNumberOfCameras();

        if (mAvailableCameras < 2) {
            cpListener.setSwitchCameraButtonVisibility(View.GONE);
        }
    }

    private Camera getCameraInstance() throws IllegalAccessException{
        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(mActualCameraId);
        } catch (Exception e) {
            e.printStackTrace();
            releaseCameraAndPreview(); //TEsting if fixes camera issues
            throw new IllegalAccessException("Cannot access camera");
        }
        return mCamera;
    }

    public void setCameraParameters() {

        if(mCamera == null) {
            getCameraInformation();
        }

        mCamera.setDisplayOrientation(90); //Fix camera rotation because the Activity won't react to device rotation changes

        Camera.Parameters parameters = mCamera.getParameters();

        setPictureSize(parameters);
        setPreviewSize(parameters);

        setFocusMode(parameters, Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        updateFlashStatus(parameters, initialFlashMode);

        updateCameraParameters(parameters);

    }

    private boolean prepareMediaRecorder() {
        boolean failed = false;

        try {
            mR.prepare();
        } catch (IOException e) {
            mCamera.lock();
            failed = true;
        }

        return failed;
    }

    //////////////////////////////////////////////Fin Inicializadores de elementos internos//////////////////////////////////////////////

    //////////////////////////////////////////////Funcionamiento de la cámara//////////////////////////////////////////////

    public void startCameraPreview() {
        startPreview();
    }

    private void startPreview() {
        if(mSurfaceTexture == null) {
            cpListener.onCameraError(new NullPointerException());
            return;
        }

        try {
            setCameraParameters();
            mCamera.setPreviewTexture(mSurfaceTexture);
            mCamera.startPreview();
            cpListener.onCameraPreviewStarted();
        } catch (Exception ex) {
            cpListener.onCameraError(ex);
        }
    }

    public int switchCamera() {

        if (mAvailableCameras > 1) {
            if (mActualCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mActualCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                cpListener.setFlashButtonVisibility(View.GONE);
            } else {
                mActualCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                cpListener.setFlashButtonVisibility(View.VISIBLE);
            }

            releaseCameraAndPreview();
            startPreview();
        }

        return mActualCameraId;
    }

    public boolean captureImage() {

        boolean captured = true;

        try {
            setPictureRotation();
            mCamera.takePicture(null, null, jpegCallback);
        } catch (Exception ex) {
            captured = false;
        }

        return captured;

    }

    public boolean recordVideo(String videoPath) {

        try {
            if (mR == null) {
                mR = new MediaRecorder();
            }

            if (!isRecording) {
                startVideoRecording(videoPath);
            } else {
                stopVideoRecording();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return isRecording;
    }

    private void startVideoRecording(String videoPath){

        this.videoPath = videoPath;

        mCamera.unlock();
        mR.setCamera(mCamera);
        mR.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mR.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mR.setProfile(getRecordQuality());
        mR.setOrientationHint(getOrientation());
        mR.setOutputFile(videoPath);

        boolean failed = prepareMediaRecorder();

        if (failed)
            failed = prepareMediaRecorder(); // Documentation indicates that when mR.prepare() fails maybe locking camera would fix it

        if (!failed) {
            mR.start();
            isRecording = true;
        }

    }

    private void stopVideoRecording(){
        try {
            mR.stop();
            isRecording = false;

            mCamera.stopPreview();
            cpListener.onVideoRecorded(videoPath);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IllegalStateException("Could not stop video capture.");
        }
    }

    public void releaseCameraAndPreview() {
        if (mCamera != null) {
            try {
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    //////////////////////////////////////////////Fin funcionamiento de la cámara//////////////////////////////////////////////

    //////////////////////////////////////////////Getters//////////////////////////////////////////////

    public int getActualCameraId() {
        return mActualCameraId;
    }

    public boolean isSurfaceAvailable(){
        return isAvailable();
    }

    public byte[] getLastImageByteArray() {
        return pictureArray;
    }

    public double getDesiredAspectRatio(){
        return this.desiredAspectRatio;
    }

    public boolean isRecording() {
        return isRecording;
    }

    public String getLastVideoName() {
        return videoName;
    }

    public String getLastVideoPath() {
        String toReturn = null;
        if (videoPath != null && !videoPath.equalsIgnoreCase("null")) {
//            toReturn = ManageFolder.SENT_VIDEOS + "/" + videoPath;
        }

        return toReturn;
    }

    private int getOrientation() {

        int orientation = 0;

        if (MyOrientationEventListener.isPortrait) {
            orientation = 90;
        } else {
            if (MyOrientationEventListener.rotation == 90) {
                orientation = 180;
            }
        }

        if (mActualCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            orientation = 270;
        }

        return orientation;

    }

    public Camera.Size getMaxPictureSize(List<Camera.Size> sizeList, double aspectRatio) {

        Camera.Size selectedSize = null;

        double evaluatingRatio = 0;
        int selectedHeight = 0;

        for(Camera.Size size : sizeList){

            if (selectedHeight < size.height) {
                evaluatingRatio = (double) size.width / size.height;
                if (evaluatingRatio == aspectRatio) {
                    selectedHeight = size.height;
                    selectedSize = size;
                }
            } else
                break;

        }

        if(selectedSize == null){
            if(sizeList.get(0).height > sizeList.get(sizeList.size() - 1).height )
                selectedSize = sizeList.get(0);
            else
                selectedSize = sizeList.get(sizeList.size() - 1);

            desiredAspectRatio = (double)selectedSize.width / selectedSize.height;
        }

        return selectedSize;
    }

    private CamcorderProfile getRecordQuality() {

        CamcorderProfile recordQuality = CamcorderProfile.get(mActualCameraId, CamcorderProfile.QUALITY_HIGH);

        int maxWidth = 0;
        int actualWidth;

        for (Camera.Size videoSize : mSupportedVideoSizes) {
            actualWidth = videoSize.width;

            if(maxWidth < actualWidth) {
                maxWidth = actualWidth;
            }
        }

        if(maxWidth > 1280) {
            recordQuality = CamcorderProfile.get(mActualCameraId, CamcorderProfile.QUALITY_480P);
            if (CamcorderProfile.hasProfile(mActualCameraId, CamcorderProfile.QUALITY_720P)) {
                recordQuality = CamcorderProfile.get(mActualCameraId, CamcorderProfile.QUALITY_720P);
            } else if (CamcorderProfile.hasProfile(mActualCameraId, CamcorderProfile.QUALITY_QVGA)) {
                recordQuality = CamcorderProfile.get(mActualCameraId, CamcorderProfile.QUALITY_QVGA);
            }
        }

        return recordQuality;

    }

    //////////////////////////////////////////////Fin Getters//////////////////////////////////////////////

    //////////////////////////////////////////////Setters//////////////////////////////////////////////

    private void setPictureRotation(){
        Camera.Parameters parameters = mCamera.getParameters();
        Camera.CameraInfo info =
                new Camera.CameraInfo();

        Camera.getCameraInfo(mActualCameraId, info);

        int rotation = 0;

        if (mActualCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
            rotation = (info.orientation - MyOrientationEventListener.orientation + 360) % 360;
        else   // back-facing camera
            rotation = (info.orientation + MyOrientationEventListener.orientation) % 360;

        lastPictureRotation = rotation;

        parameters.setRotation(rotation);
        updateCameraParameters(parameters);
    }


    public void setFocusArea(MotionEvent event, boolean canDraw) {

        float x = event.getX();
        float y = event.getY();

        Rect touchRect = new Rect(
                (int) (x - 100),
                (int) (y - 100),
                (int) (x + 100),
                (int) (y + 100));

        Rect fixedRect = fixCoordinates(touchRect);
        Matrix m = new Matrix();
        prepareMatrix(m, false, 270);

        try {
            RectF targetFocusRect = new RectF(
                    fixedRect.left * 2000 / getWidth() - 1000,
                    fixedRect.top * 2000 / getHeight() - 1000,
                    fixedRect.right * 2000 / getWidth() - 1000,
                    fixedRect.bottom * 2000 / getHeight() - 1000);

            m.mapRect(targetFocusRect);

            Rect r = new Rect();
            targetFocusRect.roundOut(r);
            doTouchFocus(r);

            if (canDraw && drawingView != null) {
                drawingView.setHaveTouch(true, touchRect);
                drawingView.invalidate();

                // Remove the square indicator after 1000 msec
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        drawingView.setHaveTouch(false, new Rect(0, 0, 0, 0));
                        drawingView.invalidate();
                    }
                }, 1500);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateLayoutParams(int width, int height, boolean createNew) {

        FrameLayout.LayoutParams params;
        if(createNew)
            params = new FrameLayout.LayoutParams(width, height);
        else {

            params = (FrameLayout.LayoutParams) getLayoutParams();
            params.height = height;
            params.width = width;

//            drawingView.getLayoutParams().height = height;
//            drawingView.getLayoutParams().width = width;

        }
        setLayoutParams(params);

    }



    private void updateCameraParameters(Camera.Parameters parameters) {
        try {
            mCamera.setParameters(parameters);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void useTVScreenRatio() {
        updateAspectRatio(PICTURE_RATIO_TVSCREEN);
    }

    public void useWideScreenRatio(){
        updateAspectRatio(PICTURE_RATIO_WIDESCREEN);
    }

    private void updateAspectRatio(double desiredAspectRatio){
        this.desiredAspectRatio = desiredAspectRatio;

        Camera.Parameters parameters = mCamera.getParameters();
        setPictureSize(parameters);
        setPreviewSize(parameters);

        updateCameraParameters(parameters);
    }

    /**
     * Call after setPictureSize
     * @param parameters
     */
    private void setPreviewSize(Camera.Parameters parameters) {
        Camera.Size previewSize = getMaxPictureSize(parameters.getSupportedPreviewSizes(), desiredAspectRatio);

        boolean isDesiredAspectRatio = ((double)previewSize.width / previewSize.height) == desiredAspectRatio;

        if(isDesiredAspectRatio) {
            updatePreviewLayout(parameters, previewSize);
        }else{

            double newDesiredAspectRatio = (double)parameters.getPictureSize().width / parameters.getPictureSize().height;

            previewSize = getMaxPictureSize(parameters.getSupportedPreviewSizes(), newDesiredAspectRatio);

            isDesiredAspectRatio = ((double)previewSize.width / previewSize.height) == newDesiredAspectRatio;

            if(isDesiredAspectRatio) {
                updatePreviewLayout(parameters, previewSize);
            }

        }

    }

    private void setPictureSize(Camera.Parameters parameters){
        Camera.Size pictureSize = getMaxPictureSize(parameters.getSupportedPictureSizes(), desiredAspectRatio);
        parameters.setPictureSize(pictureSize.width, pictureSize.height);
    }

    private void updatePreviewLayout(Camera.Parameters parameters, Camera.Size previewSize){
        parameters.setPreviewSize(previewSize.width, previewSize.height);

        int height = (int) (getWidth() * desiredAspectRatio);

        if (height != getHeight()) {
            cpListener.updateLayoutParams(getWidth(), height);
        }
    }

    private void setFocusMode(Camera.Parameters parameters, String mode){

        List<String>supportedFocusModes = parameters.getSupportedFocusModes();

        if(supportedFocusModes == null)
            return;

        if (supportedFocusModes.contains(mode))
            parameters.setFocusMode(mode);
        else
            parameters.setFocusMode(supportedFocusModes.get(0));
    }


    public void updateFlashStatus(Camera.Parameters parameters, String mode) {
        List<String> mSupportedFlashModes = parameters.getSupportedFlashModes();

        if(mSupportedFlashModes == null)
            return;

        if (mSupportedFlashModes.contains(mode))
            parameters.setFlashMode(mode);
        else
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

        if (mSupportedFlashModes != null && mSupportedFlashModes.size() < 0) {
            cpListener.setFlashButtonVisibility(View.GONE);
        }
    }

    //////////////////////////////////////////////Fin Setters//////////////////////////////////////////////

    //////////////////////////////////////////////Utilities//////////////////////////////////////////////

    private boolean checkPermissions(){
        boolean granted = true;

        for(String permission : requiredPermissions) {
            int permissionStatus = ActivityCompat.checkSelfPermission(getContext(), permission);
            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                granted = false;
                break;
            }
        }

        return granted;
    }

    private void requestPermissions(){
        cpListener.requestPermissions(requiredPermissions);
    }

    public static void prepareMatrix(Matrix matrix, boolean mirror, int displayOrientation) {

        matrix.setScale(mirror ? -1 : 1, 1);

        matrix.postRotate(displayOrientation);

    }

    private Rect fixCoordinates(Rect rectArea) {

        int left, top, right, bottom, diff, screenWidth, screenHeight;

        left = rectArea.left;
        top = rectArea.top;
        right = rectArea.right;
        bottom = rectArea.bottom;

        screenWidth = getWidth();
        screenHeight = getHeight();


        if (right > screenWidth) {
            diff = right - screenWidth;
            right = screenWidth;
            left = left - diff;
        }

        if (bottom > screenHeight) {
            diff = bottom - screenHeight;
            bottom = screenHeight;
            top = top - diff;
        }

        if (left < 0) {
            diff = left;
            left = 0;
            right = right - diff;
        }

        if (top < 0) {
            diff = top;
            top = 0;
            bottom = bottom - diff;
        }

        return new Rect(left, top, right, bottom);

    }

    public void doTouchFocus(final Rect tfocusRect) {
        try {
            List<Camera.Area> focusList = new ArrayList<>();
            Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
            focusList.add(focusArea);

            Camera.Parameters param = mCamera.getParameters();
            setFocusMode(param, Camera.Parameters.FOCUS_MODE_MACRO);

            int maxNumFocusArea = param.getMaxNumFocusAreas();
            int maxNumMeteringAreasArea = param.getMaxNumMeteringAreas();

            if(maxNumFocusArea > 0 && focusList.size() <= maxNumFocusArea)
                param.setFocusAreas(focusList);

            if(maxNumMeteringAreasArea > 0 && focusList.size() <= maxNumMeteringAreasArea)
                param.setMeteringAreas(focusList);

            updateCameraParameters(param);

            mCamera.autoFocus(myAutoFocusCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //////////////////////////////////////////////Fin Utilities//////////////////////////////////////////////

    public interface CameraPreviewListener {
        void setFlashButtonVisibility(int visibility);

        void setSwitchCameraButtonVisibility(int visibility);

        void onPictureTaken(byte[] imageByteArray, boolean fromFrontCamera);

        void onVideoRecorded(String path);

        void onSurfaceTextureAvailable();

        void requestPermissions(String[] permissions);

        void onCameraPreviewStarted();

        void onCameraError(Exception ex);

        void updateLayoutParams(int width, int height);
    }

    public interface FullScreenListener {
        void onSetFullScreen();
        void onSetHalfScreen();
    }

    public interface HandlePermission {
        void onGrantedCameraPermissions();
    }

}