package com.kodevincere.kamera;

import android.content.Context;
import android.view.OrientationEventListener;

/**
 * Created by alphonselric
 */

public class MyOrientationEventListener extends OrientationEventListener {

    public static boolean isPortrait = true;
    public static int rotation, orientation;
    private final int TRESHOLD = 20;

    public MyOrientationEventListener(Context context) {
        super(context);
    }

    @Override
    public void onOrientationChanged(int actualOrientation) {
        if (actualOrientation != ORIENTATION_UNKNOWN)
            orientation = (actualOrientation + 45) / 90 * 90;

        if (isLandscape(orientation))
            isPortrait = false;
        else if (isPortrait(orientation))
            isPortrait = true;

    }

    private boolean isLandscape(int orientation) {
        return orientation >= (90 - TRESHOLD) && orientation <= (90 + TRESHOLD) || orientation >= (270 - TRESHOLD) && orientation <= (270 + TRESHOLD);
    }

    private boolean isPortrait(int orientation) {
        return (orientation >= (360 - TRESHOLD) && orientation <= 360) || (orientation >= 0 && orientation <= TRESHOLD);
    }

}