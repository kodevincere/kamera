package com.kodevincere.kamera.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kodevincere.kamera.MyOrientationEventListener;
import com.kodevincere.kamera.R;
import com.kodevincere.kamera.config.KameraConfig;
import com.kodevincere.kamera.views.KameraView;
import com.kodevincere.kamera.views.KameraView.CameraPreviewListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by alphonselric
 */

public class KameraPreviewFragment extends Fragment implements View.OnClickListener, CameraPreviewListener {

    private String LOG_TAG = this.getClass().getCanonicalName();

    protected static final String TEMP_IMAGE = "tempImage";

    static final String[] requiredPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    //Listeners
    private OnCameraPictureSavedListener mCameraPictureSavedListener;
    private OnCameraVideoSavedListener mCameraVideoSavedListener;
    private MyOrientationEventListener mOrientationEventListener;

    //Views
    private KameraView mCamera;
    private RelativeLayout rlCameraButtonsParent;
    private Button btnBack;
    private Button btnRatio;
    private Button btnCapture;
    private Button btnSwitchCamera;

    private int mInitialCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
    private double mDesiredAspectRatio = KameraView.PICTURE_RATIO_WIDESCREEN;
    private boolean mCaptureButtonVisible;
    private boolean mSwitchCameraButtonVisible;
    private boolean mCameraStarted = false;
    private boolean mIsOverlayVisible;
    private boolean mMirrorImageInYAxisAlways;
    private boolean mMirrorImageInYAxisOnlyWhenFrontCamera;
    private String mMediaType;
    private String mVideoDestinationPath = "";

    public KameraPreviewFragment() {
        // Required empty public constructor
    }

    public static KameraPreviewFragment newInstance(Bundle b) {
        KameraPreviewFragment fragment = new KameraPreviewFragment();

        if(b != null)
            fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mCameraPictureSavedListener = (OnCameraPictureSavedListener) context;
        }catch (Exception ex){
            throw new IllegalArgumentException("Parent must implement OnCameraPictureSavedListener interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_camera_preview, container, false);

        mCameraStarted = false;
        getViews(v);
        extractConfigData();
        setButtonConfig();
        setOverlay(v);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(checkRequiredPermissions()) {
            initCamera();
            initListeners();
            setListeners();
            if (!mCameraStarted)
                startPreviewInThread();

        }else
            requestPermissions(requiredPermissions);

    }

    @Override
    public void onStop() {
        super.onStop();
        if(mOrientationEventListener != null)
            mOrientationEventListener.disable();
        if(mCamera != null)
            mCamera.releaseCameraAndPreview();
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();

        if(id == R.id.btn_back)
            onBackClicked();
        else if(id == R.id.btn_ratio)
            onRatioClicked();
        else if(id == R.id.btn_capture)
            onCaptureClicked();
        else if(id == R.id.btn_switch_camera)
            onSwitchCameraClicked();
    }

    @Override
    public void setFlashButtonVisibility(int visibility) {

    }

    @Override
    public void setSwitchCameraButtonVisibility(int visibility) {
        btnSwitchCamera.setVisibility(visibility);
    }

    @Override
    public void onPictureTaken(byte[] imageByteArray, boolean fromFrontCamera) {
        tryToSavePicture(imageByteArray);
    }

    @Override
    public void onVideoRecorded(String path) {
        mCameraVideoSavedListener.onCameraVideoSaved(path);
        startPreviewInThread();
    }

    @Override
    public void onSurfaceTextureAvailable() {
        if(checkRequiredPermissions()) {
            if (!mCameraStarted)
                startPreviewInThread();
        }
    }

    @Override
    public void requestPermissions(String[] permissions) {
        ((OnCheckPermissions) mCameraPictureSavedListener).checkPermissions(permissions);
    }

    @Override
    public void onCameraPreviewStarted() {
        mCameraStarted = false;
    }

    @Override
    public void onCameraError(Exception ex) {
        mCameraStarted = false;
        ex.printStackTrace();
    }

    @Override
    public void updateLayoutParams(final int width, final int height) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCamera.updateLayoutParams(width, height, false);
            }
        });
    }

    private void onBackClicked(){
        try {
            getActivity().onBackPressed();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void onRatioClicked(){
        disableAllButtonsTemporarily();
        changeAspectRatio();
    }

    private void onCaptureClicked(){
        if(btnCapture.getTag().equals(getString(R.string.tag_capture_picture)))
            capturePicture();
        else if(btnCapture.getTag().equals(getString(R.string.tag_record_video)))
            recordVideo();
    }

    private void onSwitchCameraClicked(){
        disableAllButtonsTemporarily();
        switchCamera();
    }

    private void getViews(View v){
        rlCameraButtonsParent = (RelativeLayout) v.findViewById(R.id.rl_camera_buttons_parent);
        mCamera = (KameraView) v.findViewById(R.id.camera_preview);
        btnBack = (Button) v.findViewById(R.id.btn_back);
        btnRatio = (Button) v.findViewById(R.id.btn_ratio);
        btnRatio.setVisibility(View.GONE);
        btnCapture = (Button) v.findViewById(R.id.btn_capture);
        btnSwitchCamera = (Button) v.findViewById(R.id.btn_switch_camera);
    }

    private void setButtonConfig(){
        KameraConfig kameraConfig = KameraConfig.getInstance();

        if(kameraConfig.isMediaPreviewButtonsBackgroundVisible())
            rlCameraButtonsParent.setBackgroundResource(R.color.black_overlay);

        if(kameraConfig.isBackButtonVisible()) {
            int resource = kameraConfig.getResourceBack();
            if(resource != KameraConfig.DEFAULT_RESOURCE){
                try{
                    resource = kameraConfig.getResourceBack();
                    btnBack.setBackgroundResource(resource);
                }catch (Exception ex){}
            }
        }else
            btnBack.setVisibility(View.GONE);

        if(mCaptureButtonVisible) {
            int resource = kameraConfig.getResourceCapture();
            if (mMediaType.equals(KameraConfig.MEDIA_TYPE_VIDEO))
                resource = kameraConfig.getResourceRecord();

            try {
                btnCapture.setBackgroundResource(resource);
            } catch (Exception ex) {
            }

        }else
            btnCapture.setVisibility(View.GONE);

        if(mSwitchCameraButtonVisible) {
            if (kameraConfig.getResourceSwitch() != KameraConfig.DEFAULT_RESOURCE) {
                try {
                    btnSwitchCamera.setBackgroundResource(kameraConfig.getResourceSwitch());
                } catch (Exception ex) {
                }
            }
        }else
            btnSwitchCamera.setVisibility(View.GONE);

    }

    private void setOverlay(View v){
        if(mIsOverlayVisible) {
            ImageView ivOverlay = (ImageView)v.findViewById(R.id.iv_overlay);
            KameraConfig config = KameraConfig.getInstance();
            if(config.getResourceOverlay() != KameraConfig.DEFAULT_RESOURCE) {
                try {
                    ivOverlay.setBackgroundResource(config.getResourceOverlay());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            ivOverlay.setVisibility(View.VISIBLE);
        }
    }

    private void extractConfigData() {
        KameraConfig config = KameraConfig.getInstance();

        mMediaType = config.getMediaType();

        mIsOverlayVisible = config.isOverlayVisible();

        mMirrorImageInYAxisAlways = config.isMirrorImageInYAxis();

        mMirrorImageInYAxisOnlyWhenFrontCamera = config.isMirrorImageInYAxisOnlyWhenFrontCamera();

        mInitialCamera = config.getInitialCamera();

        if(mInitialCamera != KameraConfig.INITIAL_CAMERA_BACK && mInitialCamera != KameraConfig.INITIAL_CAMERA_FRONT)
            mInitialCamera = KameraConfig.INITIAL_CAMERA_BACK;

        mDesiredAspectRatio = config.getAspectRatio();

        mMediaType = config.getMediaType();
        mIsOverlayVisible = config.isOverlayVisible();
        mInitialCamera = config.getInitialCamera();

        if(mInitialCamera != KameraConfig.INITIAL_CAMERA_BACK && mInitialCamera != KameraConfig.INITIAL_CAMERA_FRONT)
            mInitialCamera = KameraConfig.INITIAL_CAMERA_BACK;

        mDesiredAspectRatio = config.getAspectRatio();

        if(mMediaType.equals(KameraConfig.MEDIA_TYPE_VIDEO)) {
            if (mCameraPictureSavedListener instanceof OnCameraVideoSavedListener) {
                this.mCameraVideoSavedListener = (OnCameraVideoSavedListener) mCameraPictureSavedListener;
                btnCapture.setBackgroundResource(R.drawable.ic_videocam_white_48dp);
                btnCapture.setTag(getString(R.string.tag_record_video));
                mVideoDestinationPath = config.getDestinationPath();
                mDesiredAspectRatio = KameraView.PICTURE_RATIO_WIDESCREEN;
            }else{
                throw new IllegalArgumentException("Parent must implement OnCameraVideoSavedListener interface");
            }
        }

        mCaptureButtonVisible = config.isCaptureButtonVisible();
        mSwitchCameraButtonVisible = config.isSwitchCameraButtonVisible();
    }

    private boolean checkRequiredPermissions() {
        boolean granted = true;

        for(String permission : requiredPermissions) {
            int permissionStatus = ActivityCompat.checkSelfPermission(getContext(), permission);
            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                granted = false;
                break;
            }
        }

        return granted;
    }

    private void initCamera(){
        mCamera.setListener(this);
        mCamera.finishConstruction(KameraView.FLASH_MODE_AUTO, mDesiredAspectRatio, mInitialCamera, false);
    }

    private void changeAspectRatio(){

        Runnable r = new Runnable() {
            @Override
            public void run() {
                double actualAspectRatio = mCamera.getDesiredAspectRatio();
                if (actualAspectRatio == KameraView.PICTURE_RATIO_TVSCREEN)
                    mCamera.useWideScreenRatio();
                else
                    mCamera.useTVScreenRatio();
            }
        };

        new Thread(r).start();

    }

    private void capturePicture(){
        disableAllButtonsTemporarily();
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.captureImage();
            }
        }).start();
    }

    private void recordVideo(){

        if(mVideoDestinationPath.isEmpty())
            Log.e(LOG_TAG, "DestinationPath required");
        else {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    boolean isRecording = mCamera.recordVideo(mVideoDestinationPath);
                    if(isRecording)
                        disableVideoButtons();
                    else
                        enableAllButtons();
                }
            };
            new Thread(r).start();
        }
    }

    private void switchCamera(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                mCamera.switchCamera();
            }
        };

        new Thread(r).start();
    }

    private void startPreviewInThread() {
        mCameraStarted = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.startCameraPreview();
            }
        }).start();
    }

    private void tryToSavePicture(final byte[] imageByteArray) {

        Runnable r = new Runnable() {
            @Override
            public void run() {

                final boolean pictureSaved = savePicture(imageByteArray);

                Runnable onPictureSaved = new Runnable() {
                    @Override
                    public void run() {
                        if (pictureSaved)
                            onPictureSaved();
                        else
                            onPictureSaveError();
                    }
                };

                postToUIThread(onPictureSaved);
            }
        };

        new Thread(r).start();

    }

    private boolean savePicture(byte[] imageByteArray){

        boolean pictureSaved = false;

        try {
            deleteTempFile();

            boolean mustMirrorAlways = mMirrorImageInYAxisAlways;
            boolean mustMirrorFrontAndIsFront = mMirrorImageInYAxisOnlyWhenFrontCamera && mCamera.getActualCameraId() == Camera.CameraInfo.CAMERA_FACING_FRONT;

            if(mustMirrorAlways || mustMirrorFrontAndIsFront)
                imageByteArray = mirrorImage(imageByteArray);


            FileOutputStream fOS = getContext().openFileOutput(TEMP_IMAGE, Context.MODE_PRIVATE);

            long initialTime = System.currentTimeMillis();
            fOS.write(imageByteArray);
            Log.e(LOG_TAG, "Compresión toma: " + (System.currentTimeMillis() - initialTime));

            fOS.close();

            pictureSaved = true;
            Log.e(LOG_TAG, "El tamaño del array es: " + imageByteArray.length);
            imageByteArray = null;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return pictureSaved;
    }

    public byte[] mirrorImage(byte[] data) {

        long initialTime = System.currentTimeMillis();
        Bitmap bitmapSrc = BitmapFactory.decodeByteArray(data, 0, data.length);
        Log.e(LOG_TAG, "Decodificación toma: " + (System.currentTimeMillis() - initialTime));
        Matrix matrix = new Matrix();
        matrix.preScale(-1, 1);


        initialTime = System.currentTimeMillis();

        Bitmap bmp = Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);


        Log.e(LOG_TAG, "Crear bitmap toma: " + (System.currentTimeMillis() - initialTime));

        bitmapSrc.recycle();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        initialTime = System.currentTimeMillis();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        Log.e(LOG_TAG, "Compresión toma: " + (System.currentTimeMillis() - initialTime));

        initialTime = System.currentTimeMillis();
        bmp.recycle();

        Log.e(LOG_TAG, "Reciclaje toma: " + (System.currentTimeMillis() - initialTime));

        byte[] array = stream.toByteArray();

        try{
            stream.flush();
            stream.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }

        System.gc();
        return array;

    }

    private void initListeners(){
        initOrientatioinListener();
    }

    private void initOrientatioinListener(){
        mOrientationEventListener = new MyOrientationEventListener(getContext());
        mOrientationEventListener.enable();
    }

    private void setListeners(){
        btnBack.setOnClickListener(this);
        btnRatio.setOnClickListener(this);
        btnSwitchCamera.setOnClickListener(this);
        btnCapture.setOnClickListener(this);

        mCamera.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    mCamera.setFocusArea(event, true);
                }

                return false;
            }
        });
    }

    private void onPictureSaved() {
        this.mCameraPictureSavedListener.onCameraPictureSaved(getContext().getFilesDir().getAbsolutePath().concat(File.separator).concat(TEMP_IMAGE));
    }

    private void onPictureSaveError() {
        Toast.makeText(getContext(), "Could not get picture", Toast.LENGTH_SHORT).show();
        if(!mCameraStarted)
            startPreviewInThread();
    }

    private void disableViewTemporarily(final View v, final int time) {
        disableView(v);
        new CountDownTimer(time, time) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                enableView(v);
            }
        }.start();
    }

    private void enableAllButtons(){
        Runnable r =  new Runnable() {
            @Override
            public void run() {
                enableView(btnCapture);
                enableView(btnRatio);
                enableView(btnSwitchCamera);
            }
        };
        postToUIThread(r);
    }

    private void disableAllButtonsTemporarily(){
        Runnable r =  new Runnable() {
            @Override
            public void run() {
                disableViewTemporarily(btnSwitchCamera, 1000);
                disableViewTemporarily(btnRatio, 1000);
                disableViewTemporarily(btnCapture, 1000);
            }
        };
        postToUIThread(r);
    }

    private void disableVideoButtons(){
        Runnable r =  new Runnable() {
            @Override
            public void run() {
                disableViewTemporarily(btnCapture, 1000);
                disableView(btnRatio);
                disableView(btnSwitchCamera);
            }
        };
        postToUIThread(r);
    }

    private void disableView(final View v){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                v.setEnabled(false);
            }
        };
        postToUIThread(r);
    }

    private void enableView(final View v){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                v.setEnabled(true);
            }
        };
        postToUIThread(r);
    }

    private void deleteTempFile(){
        getContext().deleteFile(TEMP_IMAGE);
    }

    private void postToUIThread(Runnable r){
        try{
            btnCapture.post(r);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    interface OnCameraPictureSavedListener{
        void onCameraPictureSaved(String imagePath);
    }

    interface OnCameraVideoSavedListener {
        void onCameraVideoSaved(String imagePath);
    }

    interface OnCheckPermissions{
        void checkPermissions(String[] permissions);
    }

}