package com.kodevincere.kamera.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.kodevincere.kamera.R;
import com.kodevincere.kamera.config.KameraConfig;
import com.kodevincere.kamera.config.KameraConfig.OnPictureModifiedListener;
import com.kodevincere.kamera.fileutils.FileOperations;
import com.kodevincere.kamera.fileutils.ImageHelper;
import com.kodevincere.kamera.ui.KameraPreviewFragment.OnCameraPictureSavedListener;
import com.kodevincere.kamera.ui.KameraPreviewFragment.OnCameraVideoSavedListener;
import com.kodevincere.kamera.ui.KameraPreviewFragment.OnCheckPermissions;
import com.kodevincere.kamera.ui.MediaPreviewFragment.MediaActionListener;

import java.io.File;

/**
 * Created by alphonselric
 */

public class KameraActivity extends AppCompatActivity implements OnCameraPictureSavedListener, OnCameraVideoSavedListener,
        OnCheckPermissions, MediaActionListener, OnPictureModifiedListener, OnClickListener {

    private String LOG_TAG = this.getClass().getCanonicalName();

    public static final String INTENT_KEY_MEDIA_TYPE = "mMediaType";
    public static final String INTENT_KEY_SAVED_MEDIA = "savedPicture";
    public static final String INTENT_VALUE_MEDIA_TYPE_PICTURE = "picture";
    public static final String INTENT_VALUE_MEDIA_TYPE_VIDEO = "video";
    public static final String VIDEO_EXTENSION = ".mp4";
    public static final String PICTURE_EXTENSION = ".jpg";

    private final int PERMISSION_REQUEST = 128;

    private boolean mIsAutoAccept = false;
    private boolean mIsCompress = false;
    private String mMediaToShow = "";
    private String mDestinationPath = "";
    private String mMediaType = KameraConfig.MEDIA_TYPE_PICTURE;
    private PictureEditorListener pictureEditorListener;

    private Fragment actualFragment;
    private KameraPreviewFragment kameraPreviewFragment;

    public static String privateFilesDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        privateFilesDir = getFilesDir().getAbsolutePath();

        setContentView(R.layout.activity_camera);
        extractConfigData();

    }

    @Override
    protected void onResume() {
        super.onResume();

        enableScreenAlwaysOn();
        setCameraFullScreen();

        if(actualFragment == null) {

            KameraPreviewFragment kameraPreviewFragment = createCameraPreviewFragment();

            actualFragment = kameraPreviewFragment;

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.rl_fragment_container, actualFragment)
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        KameraConfig.getInstance().setPictureEditorListener(null);
        disableScreenAlwaysOn();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean allGranted = true;

        for(int i = 0; i < permissions.length; i++){

            if(grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }

        if(allGranted){
            KameraPreviewFragment kameraPreviewFragment = createCameraPreviewFragment();
            changeFragment(kameraPreviewFragment, true);
        }else
            permissionsFailed();

    }

    @Override
    public void onCameraPictureSaved(String imagePath) {
        mMediaToShow = imagePath;

        if(pictureEditorListener != null)
            pictureEditorListener.onPictureReadyToEdit(imagePath, this);
        else
            handlePictureSaved();

    }

    @Override
    public void onCameraVideoSaved(String videoPath) {
        mMediaToShow = videoPath;

        if(mIsAutoAccept)
            autoAcceptMedia();
        else
            showMediaPreviewFragment();

    }

    @Override
    public void checkPermissions(String[] permissions) {
        boolean shouldRequestPermission = false;

        for(String permission : permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                shouldRequestPermission = true;
                break;
            }
        }

        if (shouldRequestPermission)
            showPermissionsDialog();
        else
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST);

    }

    @Override
    public void onMediaAccepted(String mediaType, String mediaLocation) {
        if(mediaType.equals(KameraConfig.MEDIA_TYPE_PICTURE) && mIsCompress)
            acceptWithCompression();
        else
            acceptWithoutCompression();
    }

    @Override
    public void onMediaCancelled() {

        KameraPreviewFragment kameraPreviewFragment = this.kameraPreviewFragment;

        if(kameraPreviewFragment == null)
            kameraPreviewFragment = KameraPreviewFragment.newInstance(null);

        this.kameraPreviewFragment = kameraPreviewFragment;

        changeFragment(kameraPreviewFragment);
    }

    @Override
    public void onMediaError(int errorCode) {
        Log.e(LOG_TAG, "onMediaError: " + errorCode);
    }

    @Override
    public void onPictureModified(Bitmap modifiedBitmap) {

        ImageHelper imageHelper = new ImageHelper(mMediaToShow, mMediaToShow);

        boolean saved = imageHelper.saveImage(modifiedBitmap);
        Log.e(LOG_TAG, "Se guardó la imagen? " + saved + " en " + mMediaToShow);

        modifiedBitmap.recycle();
        
        handlePictureSaved();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btn_back)
            onBackPressed();
    }

    private void extractConfigData(){

        KameraConfig kameraConfig = KameraConfig.getInstance();

        mIsCompress = kameraConfig.isCompress();

        mIsAutoAccept = kameraConfig.isAutoAccept();

        mDestinationPath = kameraConfig.getDestinationPath();

        mMediaType = kameraConfig.getMediaType();

        if(!mDestinationPath.isEmpty()) {
            if (mMediaType.equals(KameraConfig.MEDIA_TYPE_VIDEO))
                mDestinationPath = mDestinationPath.concat(VIDEO_EXTENSION);
            else
                mDestinationPath = mDestinationPath.concat(PICTURE_EXTENSION);

            kameraConfig.setDestinationPath(mDestinationPath);
        }

        pictureEditorListener = kameraConfig.getPictureEditorListener();
    }

    private void changeFragment(Fragment fragment){
        changeFragment(fragment, false);
    }

    private void changeFragment(Fragment fragment, boolean allowStateLoss){

        int id = getContainerId();

        if(id == -1)
            id = R.id.rl_fragment_container;


        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(id, fragment);

        if(allowStateLoss)
            transaction.commitAllowingStateLoss();
        else
            transaction.commit();

        actualFragment = fragment;
    }

    private int getContainerId(){
        int id = -1;

        try {
            id = ((ViewGroup) actualFragment.getView().getParent()).getId();
        }catch(Exception ex){
        }

        return id;

    }

    private void returnMedia(String mediaType, String mediaLocation){
        Bundle b = new Bundle();
        b.putString(INTENT_KEY_SAVED_MEDIA, mediaLocation);
        b.putString(INTENT_KEY_MEDIA_TYPE, mediaType);

        Intent i = new Intent();
        i.putExtras(b);

        setResult(RESULT_OK, i);
        finish();
    }

    private void showPermissionsDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(getString(R.string.permissions_required))
                .setMessage(getString(R.string.required_permissions_message))
                .setPositiveButton(getString(R.string.dialog_settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:".concat(getApplicationContext().getPackageName())));
                        startActivity(i);
                    }
                }).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionsFailed();
                        dialog.dismiss();
                    }
                });

        dialogBuilder.show();

    }

    private KameraPreviewFragment createCameraPreviewFragment() {
        kameraPreviewFragment = KameraPreviewFragment.newInstance(null);

        return kameraPreviewFragment;
    }

    private void showMediaPreviewFragment() {
        KameraConfig config = KameraConfig.getInstance();
        Bundle b = new Bundle();
        b.putString(MediaPreviewFragment.INTENT_KEY_MEDIA, mMediaToShow);
        b.putString(MediaPreviewFragment.INTENT_KEY_MEDIA_TYPE, mMediaType);
        b.putInt(MediaPreviewFragment.INTENT_KEY_ACCEPT_RESOURCE, config.getResourceAccept());
        b.putInt(MediaPreviewFragment.INTENT_KEY_CANCEL_RESOURCE, config.getResourceCancel());
        changeFragment(MediaPreviewFragment.newInstance(b));
    }

    private void autoAcceptMedia() {
        onMediaAccepted(mMediaType, mMediaToShow);
    }

    private void acceptWithCompression(){
        if(mDestinationPath.isEmpty())
            mDestinationPath = mMediaToShow;

        Runnable r = new Runnable() {
            @Override
            public void run() {
                ImageHelper imageHelper = new ImageHelper(mMediaToShow, mDestinationPath);
                final String filePath = imageHelper.compressImage();
                Runnable imageCompressed = new Runnable() {
                    @Override
                    public void run() {
                        //TODO Manejar errores en la compresión
                        onImageCompressed(filePath);
                    }
                };

                postToUIThread(imageCompressed);
            }
        };

        new Thread(r).start();

    }

    private void acceptWithoutCompression(){
        if(mMediaToShow.equals(mDestinationPath))
            returnMedia(mMediaType, mMediaToShow);
        else
            tryToCopyFile();
    }

    private void onImageCompressed(String filePath) {
        returnMedia(mMediaType, filePath);
    }

    private void tryToCopyFile(){
        final File showingImage = new File(mMediaToShow);
        final File destinationImage = new File(mDestinationPath);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                final boolean copied = FileOperations.copyFile(showingImage, destinationImage);

                Runnable imageAccepted = new Runnable() {
                    @Override
                    public void run() {
                        if (copied)
                            returnMedia(mMediaType, mDestinationPath);
                        else
                            returnMedia(mMediaType, mMediaToShow);
                    }
                };
                postToUIThread(imageAccepted);
            }
        };
        new Thread(r).start();
    }

    private void permissionsFailed() {
        Toast.makeText(getApplicationContext(), "Todos los permisos son requeridos", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void enableScreenAlwaysOn(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void disableScreenAlwaysOn(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void setCameraFullScreen(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }

    private void handlePictureSaved(){
        if(mIsAutoAccept)
            autoAcceptMedia();
        else
            showMediaPreviewFragment();
    }

    private void postToUIThread(Runnable r){
        try{
            getWindow().getDecorView().getHandler().post(r);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public interface PictureEditorListener{
        void onPictureReadyToEdit(String picturePath, OnPictureModifiedListener pictureModifiedListener);
    }

}
