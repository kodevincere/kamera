package com.kodevincere.kamera.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kodevincere.kamera.R;
import com.kodevincere.kamera.fileutils.ImageHelper;

import java.io.File;
import java.util.List;

/**
 * Created by alphonselric
 */

public class MediaPreviewFragment extends Fragment  implements OnClickListener{

    private String LOG_TAG = this.getClass().getCanonicalName();

    //Constants
    protected final static String INTENT_KEY_MEDIA = "media";
    protected final static String INTENT_KEY_MEDIA_TYPE = "mediaType";
    protected final static String INTENT_KEY_ACCEPT_RESOURCE = "acceptResource";
    protected final static String INTENT_KEY_CANCEL_RESOURCE = "cancelResource";
    protected final static String INTENT_KEY_BUTTONS_BACKGROUND_VISIBLE = "backgroundVisible";
    protected final static String INTENT_VALUE_MEDIA_TYPE_VIDEO = "video";
    protected final static String INTENT_VALUE_MEDIA_TYPE_PICTURE = "picture";
    protected final static int ERROR_MEDIA_MISSING = 0;
    protected final static int ERROR_MEDIA_TYPE_MISSING = 1;

    //Listeners
    private MediaActionListener mediaActionListener;

    //Views
    private ImageView ivPreview2;
    private LinearLayout llPreviewButtons;
    private Button btnCancelPreview, btnAcceptPreview;
    private Button btnPlayVideo;

    //Fields
    private String mMediaType = "";
    private String mMediaToShow = "";
    private int mAcceptResource = -1;
    private int mCancelResource = -1;
    private int mWidth;
    private int mHeight;

    public MediaPreviewFragment() {
    }

    public static MediaPreviewFragment newInstance(Bundle b) {
        MediaPreviewFragment fragment = new MediaPreviewFragment();

        if(b != null)
            fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDisplayPixels();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_preview, container, false);

        getViews(v);
        extractBundleArgs();
        setPlayButtonVisibility();
        setButtonBackground();

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mediaActionListener = (MediaActionListener) context;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new IllegalArgumentException("Parent activity must implement MediaActionListener interface");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showImage();
        initListeners();
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();

        if(id == R.id.btn_accept_preview)
            onAcceptClicked();
        else if(id == R.id.btn_cancel_preview)
            onCancelClicked();
        else if(id == R.id.btn_play_video)
            onPlayClicked();
    }

    private void onAcceptClicked(){
        if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_PICTURE))
            onImageAccepted();
        else if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_VIDEO))
            onVideoAccepted();
    }

    private void onCancelClicked(){
        deleteMedia();
        mediaActionListener.onMediaCancelled();
    }

    private void onPlayClicked(){
        Intent i = createIntent();

        if(canHandleVideoIntent(i)){
            startActivity(i);
        }else{
            Toast.makeText(getContext(), "No apps can handle this media. Sorry!", Toast.LENGTH_SHORT).show();
            Log.e(LOG_TAG, "No apps can handle this media. Sorry!");
        }
    }

    private void onImageAccepted(){
        mediaActionListener.onMediaAccepted(mMediaType, mMediaToShow);
    }

    private void onVideoAccepted(){
        mediaActionListener.onMediaAccepted(mMediaType, mMediaToShow);
    }

    private void initListeners(){
        btnAcceptPreview.setOnClickListener(this);
        btnCancelPreview.setOnClickListener(this);
        btnPlayVideo.setOnClickListener(this);
    }

    private void showImage(){

        Runnable r = new Runnable() {
            @Override
            public void run() {

                if(!mMediaToShow.isEmpty()){
                    Bitmap bitmap = null;
                    if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_PICTURE))
                        bitmap = loadImageBitmap();
                    else if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_VIDEO))
                        bitmap = loadVideoBitmap();

                    final Bitmap finalBitmap = bitmap;

                    Runnable setImagePreview = new Runnable() {
                        @Override
                        public void run() {
                            ivPreview2.setImageBitmap(finalBitmap);
                        }
                    };
                    postToUIThread(setImagePreview);

                }
            }
        };
        new Thread(r).start();
    }

    private Bitmap loadImageBitmap() {
        ImageHelper imageHelper = new ImageHelper();
        Bitmap bitmap = imageHelper.loadReducedBitmap(mMediaToShow, mWidth, mHeight);
        bitmap = imageHelper.loadBitmapWithCorrectOrientation(bitmap, mMediaToShow);

        return bitmap;
    }

    private Bitmap loadVideoBitmap(){
        ImageHelper imageHelper = new ImageHelper();

        return imageHelper.loadVideoBitmap(mMediaToShow);
    }

    private void getViews(View v){
        ivPreview2 = (ImageView) v.findViewById(R.id.iv_preview2);
        llPreviewButtons = (LinearLayout) v.findViewById(R.id.ll_preview_buttons_background);
        btnCancelPreview = (Button) v.findViewById(R.id.btn_cancel_preview);
        btnAcceptPreview = (Button) v.findViewById(R.id.btn_accept_preview);
        btnPlayVideo = (Button) v.findViewById(R.id.btn_play_video);
    }

    private void getDisplayPixels(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        mWidth = displayMetrics.widthPixels;
        mHeight = displayMetrics.heightPixels;
    }

    private void extractBundleArgs(){
        Bundle b = getArguments();

        if(b.containsKey(INTENT_KEY_MEDIA))
            mMediaToShow = b.getString(INTENT_KEY_MEDIA);
        else{
            mediaActionListener.onMediaError(ERROR_MEDIA_MISSING);
            return;
        }

        if(b.containsKey(INTENT_KEY_MEDIA_TYPE))
            mMediaType = b.getString(INTENT_KEY_MEDIA_TYPE);
        else {
            mediaActionListener.onMediaError(ERROR_MEDIA_TYPE_MISSING);
            return;
        }

        if(b.getBoolean(INTENT_KEY_BUTTONS_BACKGROUND_VISIBLE, false))
            llPreviewButtons.setBackgroundResource(R.color.black_overlay);

        mAcceptResource = b.getInt(INTENT_KEY_ACCEPT_RESOURCE, -1);
        mCancelResource = b.getInt(INTENT_KEY_CANCEL_RESOURCE, -1);


    }

    private void setButtonBackground(){

        if(mAcceptResource != -1){
            try{
                btnAcceptPreview.setBackgroundResource(mAcceptResource);
            }catch (Exception ex){
            }
        }

        if(mCancelResource != -1){
            try{
                btnCancelPreview.setBackgroundResource(mCancelResource);
            }catch (Exception ex){
            }
        }

    }

    private void setPlayButtonVisibility() {

        if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_PICTURE))
            btnPlayVideo.setVisibility(View.GONE);
        else if(mMediaType.equals(INTENT_VALUE_MEDIA_TYPE_VIDEO))
            btnPlayVideo.setVisibility(View.VISIBLE);
    }

    private Intent createIntent() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mMediaToShow));
        intent.setDataAndType(Uri.parse(mMediaToShow), "video/mp4");

        return intent;
    }

    private boolean canHandleVideoIntent(Intent intent) {
        PackageManager packageManager = getContext().getPackageManager();
        List activities = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return activities.size() > 0;
    }

    private void deleteMedia(){
        File media = new File(mMediaToShow);
        try{
            boolean deleted = media.delete();
            Log.e(LOG_TAG, "Deleted? ".concat(Boolean.toString(deleted)));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void postToUIThread(Runnable r){
        try{
            ivPreview2.post(r);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public interface MediaActionListener {
        void onMediaAccepted(String mediaType, String mediaLocation);
        void onMediaCancelled();
        void onMediaError(int errorCode);
    }

}
